---
author: Krzysztof Gutkowski
title: Markdown
subtitle: Beamer
date: 14 marca 2021
theme: Warsaw
output: beamer_presentation
header-includes:
    \usepackage{xcolor}
    \usepackage{listings}
---



## Markdown

Markdown – język znaczników przeznaczony do formatowania tekstu zaprojektowany przez Johna Grubera i Aarona Swartza. Został stworzony w celu jak najbardziej uproszczonego tworzenia i formatowania tekstu.

Jest często używany między innymi w:

* blogach,
* komunikatorach tekstowych,
* forach dyskusyjnych,
* platformach kolaboracyjnych,
* dokumentacji oprogramowania,
* plikach README.

Markdown został oryginalnie stworzony w Perlu, później dostępny w wielu innych.

## Warianty

Oryginalna specyfikacja składni Markdown była dość niejednoznaczna w definiowaniu poprawnego wynikowego dokumentu na podstawie kodu źródłowego, dlatego późniejsze wersje często różniły się składnią i funkcjonalnością. Do najpopularniejszych wariantów należą:

* GitHub Flavored Markdown (GFM),
* CommonMark,
* Markdown Extra.

### Standard Markdown
Do dziś nie została wyłoniona wersja Markdown jednogłośnie uznawana jako wersja standardowa.


## Listy wypunktowane

```markdown
* element 1
* element 2
```

Wynik:

* element 1
* element 2

### Inne symbole
Zamiast symbolu `*` można też używać symboli `+`, `-`:

## Listy numerowane

```markdown
1. element 1
2. element 2
```

Wynik:

1. element 1
2. element 2

## Zagnieżdżanie elementów
Zarówno listy wypunktowane, jak i numerowane, mogą być zagnieżdzane w obu rodzajach list.

```markdown
1. element 1
2. element 2
    1. element 1.1
        - element zagnieżdżony
```

1. element 1
2. element 2
    1. element 1.1
        - element zagnieżdżony

## Formatowanie tekstu

```markdown
Tekst może być **wytłuszczony**, *napisany kursywą*,
_podkreślony_, lub ~~przekreślony~~.
```

Wynik:

Tekst może być **wytłuszczony**, *napisany kursywą*, _podkreślony_, lub ~~przekreślony~~.
